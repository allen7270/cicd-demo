# 使用一個輕量級的 Nginx 映像作為基礎映像
FROM nginx:alpine

# 複製你的 HTML 檔案到 Nginx 的預設網站目錄
COPY . /usr/share/nginx/html

WORKDIR /usr/share/nginx/html

# 暴露 Nginx 的預設網站端口
EXPOSE 80

# 預設的執行指令
CMD ["nginx", "-g", "daemon off;"]